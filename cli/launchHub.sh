#!/bin/bash

SELENIUM_ROOT_DIR="$( cd "$( dirname "$0" )"/../ && pwd )"

source $SELENIUM_ROOT_DIR/config.sh

for D in /var/log/suho/ /var/lock/suho/; do
    if [ ! -d /var/log/suho/ ]; then
        echo "Making directory $D"
        mkdir /var/log/suho/ || exit 1
        chgrp suhodev /var/log/suho || exit 1
        chmod g+rwX /var/log/suho || exit 1
    fi
done

java -jar "$SELENIUM_SERVER_LOCAL_JAR_PATH" -role hub >> $LOG_DIR/selenium-hub.log 2>&1
