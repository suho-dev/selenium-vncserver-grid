#!/bin/bash

echo "HELLO"
exit 1

SELENIUM_ROOT_DIR="$( cd "$( dirname "$0" )"/../ && pwd )"

WORKERS_TO_START=${WORKERS_TO_START:=4}

for I in `seq 1 $WORKERS_TO_START`; do 
    export NODE_PORT=$((5555 - 1 + $I))
    export DISPLAY_TO_START=:$((5921 - 1 + $I))
    export THE_USERNAME=selwork$I

    id -u $THE_USERNAME > /dev/null 2>&1 || (
        echo "Adding the user, $THE_USERNAME.selmworkers"
        groupadd selmworkers > /dev/null 2>&1
        useradd --create-home -g selmworkers $THE_USERNAME -s /bin/bash || exit 1
    ) || exit 1

    echo "Starting a worker on listening on port $NODE_PORT, running in the X-Display, $DISPLAY_TO_START using the user, $THE_USERNAME"
    su $THE_USERNAME -c $SELENIUM_ROOT_DIR/cli/launchWorker.sh  || exit 1
done

function do_something() {
    echo "signal got"
}

echo "Now Waiting"
trap do_something EXIT

wait
echo "Continuing"
