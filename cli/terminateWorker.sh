#!/bin/bash

echo "Stopping the vnc session ran by `whoami` on $DISPLAY_TO_STOP"
vncserver -kill $DISPLAY_TO_STOP
