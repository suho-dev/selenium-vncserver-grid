#!/bin/bash

cp /suho/selenium-grid/etc-init/selenium-hub.conf /etc/init/selenium-hub.conf
ln -s /lib/init/upstart-job /etc/init.d/selenium-hub

sudo apt-get install -y ratpoison

cp /suho/selenium-grid/etc-init/selenium-workers.conf /etc/init/selenium-workers.conf
ln -s /lib/init/upstart-job /etc/init.d/selenium-workers

ln -s /suho/selenium-grid/etc-init/selenium-grid.conf /etc/init/selenium-grid.conf
